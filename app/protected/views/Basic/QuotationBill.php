<?php

include_once '../MPDF57/mpdf.php';

$pdf = new mPDF("UTF-8");
$pdf->SetAutoFont();

$created_at = Util::DateThai($quotation->created_at);

// style
$html = "
<style>
  .cell-header {
    border-top: #808080 1px solid;
    border-bottom: #808080 3px double;
  }
  .cell-body {
    border-bottom: #808080 1px solid;
  }
  .cell-footer {
    border-bottom: #808080 1px solid;
  }
  .cell {
    border: #808080 1px solid;
    padding: 10px;
  }
</style>
";

$html .= "<div style='text-align: center; font-weight: bold;'>ใบเสนอราคา</div>";
$html .= "<div style='margin-top: 10px; text-align: right'>เลขที่: {$quotation->id}</div>";
$html .= "<div style='text-align: right'>วันที่: {$created_at}</div>";
$html .= "<div style='margin-top: 2px;'>เรียนคุณ {$quotation->customer_name}</div>";
$html .= "
  <div style='margin-top: 10px;'>
  &nbsp;&nbsp;&nbsp;&nbsp;
  ข้าพเจ้า {$org->org_name} ที่อยู่ {$org->org_address_1} {$org->org_address_2} {$org_address_3} {$org_address_4} โทรศัพท์ {$org->org_tel} เลขประจำตัวผู้เสียภาษี {$org->org_tax_code} ขอเสนอราคาพัสดุดังรายการต่อไปนี้
  </div>";
$html .= "
  <table style='margin-top: 15px' width='100%' cellpadding='5px' cellspacing='0px'>
    <thead>
      <tr>
        <th class='cell-header'>ลำดับ</th>
        <th class='cell-header'>รายการ</th>
        <th class='cell-header'>จำนวน</th>
        <th class='cell-header'>หน่วยละ</th>
        <th class='cell-header'>ส่วนลด</th>
        <th class='cell-header'>คงเหลือ</th>
      </tr>
    </thead>
    <tbody>";
    $n = 1;
    $sum = 0;
    $sumSub = 0;
    $sumOldPrice = 0;
    $sumSalePrice = 0;

    foreach ($quotationDetails as $quotationDetail) {
      $qty = number_format($quotationDetail->qty);
      $oldPrice = number_format($quotationDetail->old_price);
      $sub = number_format($quotationDetail->sub);
      $salePrice = number_format($quotationDetail->sale_price);

      $sum += ($quotationDetail->qty * $quotationDetail->sale_price);
      $sumSub += $quotationDetail->sub;
      $sumOldPrice += $quotationDetail->old_price;
      $sumSalePrice += $quotationDetail->sale_price;

      $html .= "
      <tr>
        <td class='cell-body' style='width: 50px; text-align: right'>{$n}</td>
        <td class='cell-body'>{$quotationDetail->getProduct()->product_name}</td>
        <td class='cell-body' style='width: 50px; text-align: right'>{$qty}</td>
        <td class='cell-body' style='width: 70px; text-align: right'>{$oldPrice}</td>
        <td class='cell-body' style='width: 70px; text-align: right'>{$sub}</td>
        <td class='cell-body' style='width: 70px; text-align: right'>{$salePrice}</td>
      </tr>
      ";

      $n++;
    }

$sumOldPrice = ($sumOldPrice - $sumSub);
$sumSalePrice = $sumOldPrice + (($sumOldPrice * $quotation->vat) / 100);

$sumSubText = number_format($sumSub);
$sumOldPriceText = number_format($sumOldPrice);
$sumSalePriceText = number_format($sumSalePrice);
$thaiMoney = Util::convertNumberToText(floor($sumSalePrice));

$html .= "
    </tbody>
    <tfoot>
      <tr>
        <td colspan='3'></td>
        <td colspan='2'>รวม</td>
        <td class='cell-footer' style='text-align: right'>{$sumSalePriceText}</td>
      </tr>
      <tr>
        <td colspan='3'></td>
        <td colspan='2'>ส่วนลด</td>
        <td class='cell-footer' style='text-align: right'>{$sumSubText}</td>
      </tr>
      <tr>
        <td colspan='3'></td>
        <td colspan='2'>มูลค่าสินค้า</td>
        <td class='cell-footer' style='text-align: right'>{$sumOldPriceText}</td>
      </tr>
      <tr>
        <td colspan='3'></td>
        <td colspan='2'>ภาษีมูลค่าเพิ่ม (%)</td>
        <td class='cell-footer' style='text-align: right'>{$quotation->vat}</td>
      </tr>
      <tr>
        <td colspan='3'></td>
        <td colspan='2'>สุทธิ</td>
        <td class='cell-footer' style='text-align: right; border-bottom: #808080 3px double'>{$sumSalePriceText}</td>
      </tr>
      <tr>
        <td colspan='6'>รวมเป็นเงินทั้งสิ้น {$thaiMoney} บาท</td>
      </tr>
    </tfoot>
  </table>";

if (empty($quotation->quotation_pay)) {
  $quotation->quotation_pay = "เงินสด";
} else {
  if ($quotation->quotation_pay != "cash") {
    $quotation->quotation_pay = "เงินเชื่อ";
  } else {
    $quotation->quotation_pay = "เงินสด";
  }
}

$html .= "
  <table width='100%'>
    <tr>
      <td class='cell' style='width: 200px;'>ยืนราคา {$quotation->quotation_day} วัน</td>
      <td class='cell'>ลูกค้า</td>
      <td class='cell'>ผู้เสนอราคา</td>
    </tr>
    <tr valign='top'>
      <td class='cell'>
        <div>กำหนดส่งของภายใน {$quotation->quotation_send_day} วัน</div>
        <div>นับตั้งแต่วันที่ลงนามในสัญญา</div>
      </td>
      <td class='cell' rowspan='2'>
        <div>ลงชื่อ</div>
        <div style='text-align: center; display: inline-block;'>
          <br />
          <br />
          <br />
          <br />
          ผู้ตกลงราคา
        </div>
      </td>
      <td class='cell' rowspan='2'>
        <div>ลงชื่อ</div>
        <divstyle='text-align: center; display: inline-block;'>
          <br />
          <br />
          <br />
          <br />
          ผู้เสนอราคา
        </div>
      </td>
    </tr>
    <tr>
      <td class='cell'>เงื่อนไขการชำระเงิน {$quotation->quotation_pay}</td>
    </tr>
  </table>
";

$pdf->WriteHTML($html);
$pdf->Output();

?>
