<style>
  #textSum {
    background-color: #808080;
    color: greenyellow;
    font-size: 23px;
    font-weight: bold;
    border: #000000 1px solid;
    text-align: right;
    padding-right: 5px;
    padding-top: 2px;
    padding-bottom: 2px;
    display: inline-block;
    width: 120px;
  }

  .mynav {
    border-bottom: #cccccc 1px solid;
    padding: 0px;
    display: inline-block;
    width: 100%;
    background: #f2f5f6;
  }

  .mynav ul li a {
    padding: 10px;
  }

  .mynav ul li {
    padding: 0px;
  }
</style>

<script type="text/javascript">
  function browseProduct() {
    $("#myModalLabel").text("เลือกสินค้า");
    $("#modalContent").html("");

    $.ajax({
      url: 'index.php?r=Dialog/DialogProduct',
      success: function(data) {
        $("#modalContent").html(data);
      }
    });

    return false;
  }

  function browseMember() {
    $("#myModalLabel").text("เลือกสมาชิกร้าน");
    $("#modalContent").html("");

    $.ajax({
      url: 'index.php?r=Dialog/DialogMember',
      success: function(data) {
        $("#modalContent").html(data);
        $(".cmdChooseMember").click(function() {
          var code = $(this).attr("member_code");
          var name = $(this).attr("member_name");

          $("input[name=member_code]").val(code);
          $("input[name=txt_member_name]").val(name);

          $("#member_code").val(code);
          $("#member_name").val(name);
        });
      }
    });

    return false;
  }

  function endSale() {
		$.ajax({
			url: 'index.php?r=Basic/EndSale',
			type: 'POST',
			data: $("#formSale").serialize(),
			success: function(data) {
				if (data == 'success') {
          doSaleReset();
				}
			}
		});
  }

  function dialogEndSale() {
    var sale_status = $("input[name=sale_status][value=credit]");
    var checked = sale_status.prop("checked");

    if (!checked) {
			$("#myModal").modal("show");
      $("#myModalLabel").text("จบการขาย");
      $("#modalContent").html("");

      $.ajax({
        url: 'index.php?r=Dialog/DialogEndSale',
        success: function(data) {
          $("#modalContent").html(data);

          var txtSum = $("#textSum").text();
          var totalMoney = $("#totalMoney");

          totalMoney.val(txtSum);
        }
      });
    } else {
      var member_code = $("#member_code").val();

      if (member_code == "") {
        alert("โปรดทำการเลือกลูกค้า เพื่อบันทึกเป็นลูกหนี้");
      } else {
        alert("จบการขายแบบไม่ได้ชำระเป็นเงินสด เรียบร้อยแล้ว");
        
        endSale();
      }
    }

    return false;
  }

  function saleReset() {
    if (confirm('เริ่มการขายใหม่')) {
      doSaleReset();
    }
  }

  function doSaleReset() {
    $("#formSale").attr('action', 'index.php?r=Basic/SaleReset').submit();
  }

  function printSlip() {
    var uri = "index.php?r=Dialog/DialogPrintSlip";
    var options = "width=360px; height=550px";
    var w = window.open(uri, null, options);
  }

  function printBillSendProduct() {
    var uri = "index.php?r=Dialog/DialogBillSendProduct";
    var options = "width=800px; height=650px";
    var w = window.open(uri, null, options);
  }

  function printBillTax() {
    var uri = "index.php?r=Dialog/DialogBillAddVat";
    var options = "width=800px; height=650px";
    var w = window.open(uri, null, options);
  }

  function findMemberName() {
    var member_code = $("#member_code").val();

    if (member_code != "") {
      $.ajax({
        url: 'index.php?r=Ajax/SearchMemberByMemberCode',
        data: {
          member_code: member_code
        },
        dataType: 'json',
        success: function(data) {
          $("#member_name").val(data.member_name);
          $("input[name=member_code]").val(data.member_code);
          $("input[name=member_name]").val(data.member_name);
        }
      });
    }
  }

  $(function() {
		// change color of table
		renderGrid();
    //reverenceCode();
    computeSum();
    initCalendar();
    findMemberName();

    <?php if (!empty($_GET['end_sale'])): ?>
    doSaleReset();
    <?php endif; ?>

    $("#product_code").keydown(function(e) {
      if (e.keyCode == 17) {
        // Control
        var sale_status = $("input[name=sale_status][value=credit]");
        var checked = sale_status.prop("checked");

        if (!checked) {
          $("#myModal").modal("show");
          dialogEndSale();
        }
      } else if (e.keyCode == 13) {
        // ENTER
        if ($(this).val().length > 0) {
          document.formSale.submit();
        }
      } else if (e.keyCode == 9) {
        // TAB
        if ($(this).val().length > 0) {
          document.formSale.submit();
        }
      }
    });

    $("#member_code").keydown(function(e) {
      if (e.keyCode == 9) {
        var member_code = $(this).val();

        $.ajax({
          url: 'index.php?r=Ajax/SearchMemberByMemberCode',
          data: {
            member_code: member_code
          },
          dataType: 'json',
          success: function(data) {
            $("#member_name").val(data.member_name);
            $("input[name=member_code]").val(data.member_code);
            $("input[name=member_name]").val(data.member_name);
          }
        });
      }
    });
  });

  function renderGrid() {
    $(".table").removeClass("table-striped");
		$(".table thead tr th")
			.css("background", "#000000")
			.css("font-weight", "normal")
			.css("color", "#f9f8f7");
		$(".table tbody tr:odd").css('background', '#cfcfcf');
		$(".table tbody tr:even").css('background-color', '#afafaf');
  }

  function computePrice(id_price, id_qty, id_output) {
    var output = $("#" + id_output);
    var price = $("#" + id_price);
    var qty = $("#" + id_qty);

    // compute row
    price = parseFloat(price.val());
    qty = parseFloat(qty.val());

    var totalPrice = (price * qty);

    var varOutput = numeral(totalPrice).format('0,0.00');
    output.text(varOutput);

    // compute sum
    computeSum();

    // save to session
    saveDataOnGrid();
  }

  function computeSum() {
    var lblSumQty = $("#lblSumQty");
    var lblSumPrice = $("#lblSumPrice");
    var sumQty = 0;
    var sumPrice = 0;

    // อ่านค่าแถวทั้งหมดใน Grid
    $("table.items tbody tr").each(function(data) {
      var tr = $(this);

      // ผลรวมของ จำนวน
      tr.find("input.qty").each(function(data) {
        var txtQty = $(this);

        sumQty += parseFloat(txtQty.val());
      });

      // ผลรวมของ ราคา
      tr.find("span.pricePerRow").each(function(data) {
        var txtPricePerRow = $(this);
        var pricePerRow = txtPricePerRow.text();
        var pricePerRow = pricePerRow.replace(",", "");

        sumPrice += parseFloat(pricePerRow);
      });
    });

    var outputSumQty = numeral(sumQty).format('0, 0');
    var outputSumPrice = numeral(sumPrice).format('0, 0.00');

    // แสดงค่าการคำนวน
    lblSumQty.text(outputSumQty);
    lblSumPrice.text(outputSumPrice);

    // แสดงผลการคำนวน ด้านบนซ้าย
    $("#textSum").text(outputSumPrice);

    // แสดงผลการคำนวน ด้านล่างสุด (ส่วนของการคำนวน ภาษี)
    $("#priceTotal").val(outputSumPrice);

    // คำนวนภาษี
    computeVat();
  }

  function saveDataOnGrid() {
    $.ajax({
      url: 'index.php?r=Ajax/SaleSaveOnGrid',
      type: 'POST',
      data: $("#formGrid").serialize(),
      success: function(data) {
      }
    });
  }

  window.onload = function() {
    $("input[name=product_code]").focus();
  }

  function computeVat() {
    var priceTotal = $("#priceTotal").val().replace(",", "");

    var total = Number(priceTotal);
    var vat = parseFloat(total * 0.07);
    var noVat = parseFloat(total - (vat));

    vat = numeral(vat).format('0, 0.00');
    noVat = numeral(noVat).format('0, 0.00');

    $("#priceVat").val(vat);
    $("#priceNoVat").val(noVat);
  }

  function computeNoVat() {
    var total = $("#priceTotal").val();

    $("#priceVat").val(0);
    $("#priceNoVat").val(total);
  }

  function printBill() {
    var uri = "index.php?r=Dialog/DialogBillSendProduct&bill_type=sale";
    var options = "width=800px; height=650px";
    var w = window.open(uri, null, options);
  }

  var charCode = 0;

  var totalFormular = 0;

  function reverenceCode() {
    $("body").keyup(function(e) {
      var up = 38;
      var down = 40;
      var left = 37;
      var right = 39;
      var space = 32;
      var enter = 13;

      var k = e.keyCode;
      var formular = (up + up + down + down + left + right + left + right + space + enter);

      totalFormular += k;

      if (totalFormular == formular) {
        var html = "";
        html += "<table width='100%'>";
        html += "<tr valign='top'>";
        html += "<td width='110px'><img style='border: 1px #808080 solid;' src='images/tavon_final_resume.jpg' width='150px' /></td>";
        html += "<td>";
        html += "<div class='pull-right'>";
        html += "<div>โปรแกรมนี้ถูกออกแบบและสร้างขึ้นโดย ผม ถาวร ศรีเสนพิลา ผู้ก่อตั้งเว็บ javathailand.com และเว็บ pingpongsoft.com รวมถึงเว็บอื่นๆ อีกมากมาย</div>";
        html += "<div>โปรแกรมนี้สร้างขึ้นในปี ค.ศ 2010 ด้วยการกัดก้อนเกลือกิน และมาม่าหักครึ่ง</div>";
        html += "<div>ไม่ว่าวันที่คุณเห็นข้อความนี้ ผมจะยังอยู่ หรือจากโลกนี้ไปแล้ว</div>";
        html += "<div>แต่อย่างน้อยคุณคือคนที่สามารถพบหน้าต่างรหัสคารวะที่ผมฝังไว้</div>";
        html += "<div>ขอฝากคติประจำตัวผมเอาไว้ด้วยนะครับ \"เดินไปข้างหน้าแม้ล้มเซ ดีกว่ายืนทำเท่ อยู่กับที่\"</div>";
        html += "</td>";
        html += "</tr>";
        html += "</table>";

        $("#myModal").modal("show");
        $("#myModalLabel").text("ยินดีด้วย นี่คือรหัสคารวะ สุดยอดหน้าต่างลับของโปรแกรมนี้");
        $("#modalContent").html(html);

        totalFormular = 0;
      }
    });
  }

  function chooseProduct(product_code) {
    $("#product_code").val(product_code);
    document.formSale.submit();
  }

  <?php if (Yii::app()->session['sessionBillSale']['bill_sale_vat'] == 'no'): ?>
  $(function() {
    computeNoVat();
  });
  <?php endif; ?>
</script>

<?php $sumTotalPrice = BillSale::sumTotalPrice(); ?>
<?php $branchList = Branch::getOptions(); ?>

<?php if (count($branchList) == 0): ?>
  <script>
    $(function() {
      alert('เนื่องจากคุณยังไม่ได้ป้อนสาขา โปรดระบุสาขาก่อน');
      window.location = 'index.php?r=Config/BranchIndex';
    });
  </script>
<?php endif; ?>

<table width="100%">
  <tr valign="top">
    <td>
    <div class="panel panel-primary" style="margin: 10px; margin-right: 5px">
      <div class="panel-heading">ขายสินค้าหน้าร้าน</div>
      <div class="navbar-primary mynav">
        <ul class="nav navbar-nav">
          <li>
            <a href="#" onclick="saleReset()">
              <i class="glyphicon glyphicon-refresh"></i>
              เริ่มการขายใหม่
            </a>
          </li>
          <li>
            <a href="#" onclick="return dialogEndSale()" data-toggle="modal" data-target="#myModal">
              <i class="glyphicon glyphicon-ok-sign"></i>
              จบการขาย
            </a>
          </li>
          <li>
            <a href="#" onclick="printBillSendProduct()">
              <i class="glyphicon glyphicon-list-alt"></i>
              พิมพ์ใบส่งสินค้า
            </a>
          </li>
          <li>
            <a href="#" onclick="printBillTax()">
              <i class="glyphicon glyphicon-file"></i>
              พิมพ์ใบกำกับภาษี
            </a>
          </li>
          <li>
            <a href="#" onclick="printSlip()">
              <i class="glyphicon glyphicon-sd-video"></i>
              พิมพ์สลิป
            </a>
          </li>
          <li>
            <a href="#" onclick="printBill()">
              <i class="glyphicon glyphicon-sd-video"></i>
              พิมพ์ใบเสร็จ
            </a>
          </li>
        </ul>
      </div>

      <div class="panel-body">

        <!-- form -->
        <?php
        $form = $this->beginWidget('CActiveForm', array(
          'htmlOptions' => array(
            'name' => 'formSale',
            'id' => 'formSale',
            'class' => 'form-inline'
          )
        ));

        $form->errorSummary($model);
        ?>
        <table width="100%">
          <tr>
            <td>
              <?php
              echo $form->labelEx($model, 'bill_sale_vat', array(
                  'style' => 'width: 80px'
              ));
              ?>
              
              <span class="alert alert-info" style="padding: 8px; display: inline-block">
                <?php
                $sessionBillSale = Yii::app()->session['sessionBillSale'];

                if (!empty($sessionBillSale)) {
                  $billSaleVat = @$sessionBillSale['bill_sale_vat'];
                }

                $radio1 = true;
                $radio2 = false;

                if (!empty($billSaleVat)) {
                  if ($billSaleVat == 'no') {
                    $radio1 = false;
                    $radio2 = true;
                  }
                }
                ?>

                <?php
                echo CHtml::radioButton('bill_sale_vat', $radio1, array(
                    'value' => 'vat',
                    'onclick' => 'computeVat()'));
                ?> คิด VAT

                <?php
                echo CHtml::radioButton('bill_sale_vat', $radio2, array(
                    'value' => 'no',
                    'onclick' => 'computeNoVat()'));
                ?> ไม่คิด VAT
              </span>

              <label style="width: 100px">รูปแบบการขาย</label>
              <span class="alert alert-danger" style="padding: 8px; display: inline-block">
                <?php
                if (!empty($sessionBillSale)) {
                  $saleAuto = @$sessionBillSale['sale_auto'];
                }

                $radio1 = true;
                $radio2 = false;

                if (!empty($saleAuto)) {
                  if ($saleAuto == 'no_auto') {
                    $radio1 = false;
                    $radio2 = true;
                  }
                }
                ?>
                <?php echo CHtml::radioButton('sale_auto', $radio1, array('value' => 'auto')); ?>
                ขายอัตโนมัติ

                <?php echo CHtml::radioButton('sale_auto', $radio2, array('value' => 'no_auto')); ?>
                กำหนดจำนวนก่อน
              </span>

              <label style="width: 100px">การชำระเงิน</label>
              <span class="alert alert-success" style="padding: 8px; display: inline-block">
                <?php
                if (!empty($sessionBillSale)) {
                  $saleStatus = @$sessionBillSale['sale_status'];
                }

                $radio1 = true;
                $radio2 = false;

                if (!empty($saleStatus)) {
                  if ($saleStatus == 'credit') {
                    $radio1 = false;
                    $radio2 = true;
                  }
                }
                ?>
                <?php echo CHtml::radioButton('sale_status', $radio1, array('value' => 'cash')); ?> เงินสด
                <?php echo CHtml::radioButton('sale_status', $radio2, array('value' => 'credit')); ?> เงินเชื่อ
              </span>

            </td>

            <td style="vertical-align: top">
              <label style="font-size: 20px; width: 50px">รวม: </label>
              <span id="textSum"><?php echo number_format($sumTotalPrice); ?></span>
            </td>
          </tr>
        </table>

        <div>
          <?php
          if (!empty($sessionBillSale['BillSale'])) {
            $branchId = $sessionBillSale['BillSale']['branch_id'];
            $model['branch_id'] = $branchId;
          }
          ?>
          <?php
          echo $form->labelEx($model, 'branch_id', array(
              'style' => 'width: 80px'
          ));
          ?>
          <?php
          echo $form->dropdownlist($model, 'branch_id', $branchList, array(
              'class' => 'form-control',
              'style' => 'width: 200px'
          ));
          ?>

          <?php
          echo $form->labelEx($model, 'bill_sale_created_date', array(
              'style' => 'width: 120px'
          ));
          ?>
          <?php
          $created_date = date("d/m/Y");

          if (!empty(Yii::app()->session['billSaleCreatedDate'])) {
            $created_date = Yii::app()->session['billSaleCreatedDate'];
          }
          echo $form->textField($model, 'bill_sale_created_date', array(
              'value' => $created_date,
              'class' => 'form-control calendar',
              'style' => 'width: 100px'
          ));

          $member_code = "";
          $member_name = "";

          if (!empty($sessionBillSale['member_code'])) {
            $member_code = $sessionBillSale['member_code'];
          }
          if (!empty($sessionBillSale['member_name'])) {
            $member_name = $sessionBillSale['member_name'];
          }
          ?>

          <?php
            if (!empty($sessionBillSale)) {
              $sale_condition = @$sessionBillSale['sale_condition'];
            }

            $radio1 = true;
            $radio2 = false;

            if (!empty($sale_condition)) {
              if ($sale_condition == 'many') {
                $radio1 = false;
                $radio2 = true;
              }
            }
          ?>

          <label>เงื่อนไขการขาย</label>
          <span class="alert alert-info" style="padding: 8px">
            <?php echo CHtml::radioButton('sale_condition', $radio1, array('value' => 'one')); ?> ขายปลีก
            <?php echo CHtml::radioButton('sale_condition', $radio2, array('value' => 'many')); ?> ขายส่ง
          </span>
        </div>

        <div>
          <div class="form-search" style="padding-top: 5px">
            <label style="width: 80px">สมาชิก</label>
            <input type="hidden" name="member_code" value="<?php echo $member_code; ?>" />
            <input type="hidden" name="member_name" value="<?php echo $member_name; ?>" />
            <input type="text"
                   id="member_code"
                   name="txt_member_code"
                   value="<?php echo $member_code; ?>"
                   class="form-control"
                   style="width: 100px" />
            <input type="text"
                   id="member_name"
                   name="txt_member_name"
                   value="<?php echo $member_name; ?>"
                   disabled="disabled"
                   class="form-control"
                   style="width: 300px" />
            <a href="#" class="btn btn-primary" onclick="return browseMember()"
              data-toggle="modal" data-target="#myModal">
              <i class="glyphicon glyphicon-search"></i>
              ...
            </a>
          </div>
        </div>

        <div style="padding-top: 5px">
          <div class="form-search">
            <label style="width: 80px">รหัสสินค้า</label>
            <input type="text"
                   name="product_code"
                   id="product_code"
                   class="form-control"
                   style="width: 200px"
                   />
            <a href="#" class="btn btn-primary" onclick="return browseProduct()"
              data-toggle="modal" data-target="#myModal">
              <i class="glyphicon glyphicon-search"></i>
              ...
            </a>

            <label style="width: 50px">จำนวน</label>
            <input type="text"
                   name="product_qty"
                   value="1"
                   class="form-control"
                   style="width: 70px"
                   />
            <a href="#" class="btn btn-primary" onclick="document.formSale.submit()">
              <i class="glyphicon glyphicon-floppy-disk"></i>
              บันทึก
            </a>

            <label style="width: 100px">serial code</label>
            <input type="text" name="product_serial_no" class="form-control"
                   style="width: 120px" />
            <label style="width: 110px">วันหมดประกัน</label>
            <input type="text" name="product_expire_date" class="form-control calendar"
                   style="width: 117px" />
          </div>
        </div>

          <div class="" style="background-color: white;">
            <table class="table table-bordered table-striped items" width="100%">
              <thead style="background: #cccccc">

                <tr>
                  <th width="30px">ลำดับ</th>
                  <th width="130px">รหัสสินค้า</th>
                  <th width="130px">serial no</th>
                  <th>ชื่อรายการ</th>
                  <th width="50px">ราคา</th>
                  <th width="50px">จำนวน</th>
                  <th width="100px">จำนวนต่อแพค</th>
                  <th width="50px">รวม</th>
                  <th width="30px"></th>
                </tr>

              </thead>
              <tbody>

                <?php
                $billSaleDetail = Yii::app()->session['billSaleDetail'];

                $sum_qty = 0;
                $sum_price = 0;
                $sum_qty_per_pack = 0;

                if (!empty($billSaleDetail)):
                  $row = 0;
                  ?>

                  <?php foreach ($billSaleDetail as $r): ?>
                    <?php
                    $qty = $r['product_qty'];
                    $price = $r['product_price'];

                    $sum_qty += $qty;
                    $sum_price += $price * $qty;
                    $sum_row = ($qty * $price);
                    $sum_qty_per_pack += $r['product_qty_per_pack'];
                    ?>

                    <tr style="background: #ffffcc">
                      <td style="text-align: right; padding: 2px"><?php echo++$row; ?></td>
                      <td style="padding: 2px">
                        <input 
                          type="hidden" 
                          name="hidden_product_codes[]"
                          value="<?php echo $r['product_code']; ?>" />
                        <?php echo $r['product_code']; ?>
                        <input 
                          type="hidden"
                          name="hidden_product_name[]"
                          value="<?php echo $r['product_name'] ?>"
                        />
                      </td>
                      <td style="padding: 2px">
                        <input
                          type="text"
    											style="border: #808080 1px solid; width: 100%; padding-left: 5px"
                          value="<?php echo $r['product_serial_no']; ?>"
                          id="txtSerialNo_<?php echo $row; ?>"
                          name="serials[]"
                          />
                      </td>
                      <td style="padding: 2px"><?php echo $r['product_name']; ?></td>
                      <td style="padding: 2px" align="right">
                        <input
                          type="text"
                          class="price"
                          style="border: #808080 1px solid; text-align: right; width: 70px; padding-right: 5px"
                          value="<?php echo $price; ?>"
                          id="txtPrice_<?php echo $row; ?>"
                          name="prices[]"
                          onkeyup="computePrice(
                    				'txtPrice_<?php echo $row; ?>',
                    				'txtQty_<?php echo $row; ?>',
                    				'lblTotalPricePerRow_<?php echo $row; ?>'
                    				)"
                          />
                      </td>
                      <td style="padding: 2px" align="right">
                        <input
                          type="text"
                          class="qty"
                          style="border: #808080 1px solid; text-align: right; width: 50px; padding-right: 5px"
                          value="<?php echo $qty; ?>"
                          id="txtQty_<?php echo $row; ?>"
                          name="qtys[]"
                          onkeyup="computePrice(
                    				'txtPrice_<?php echo $row; ?>',
                    				'txtQty_<?php echo $row; ?>',
                    				'lblTotalPricePerRow_<?php echo $row; ?>'
                    				)"
                          />
                      </td>
                      <td style="padding: 2px" align="right">
                        <?php echo number_format($r['product_qty_per_pack']); ?>
                        <input type="hidden" name="hidden_qty_per_pack[]" value="<?php echo $r['product_qty_per_pack']; ?>" />
                      </td>
                      <td style="padding: 2px" align="right">
                        <span id="lblTotalPricePerRow_<?php echo $row; ?>"
                              class="pricePerRow">
                                <?php echo number_format($sum_row, 2); ?>
                        </span>
                      </td>
                      <td style="padding: 2px">
    										<a href="index.php?r=Basic/SaleDelete&index=<?php echo $row - 1; ?>"
    											class="btn btn-danger btn-xs">
    											<b class="glyphicon glyphicon-remove"></b>
    										</a>
                      </td>

                    </tr>

                  <?php endforeach; ?>
                <?php endif; ?>

              </tbody>
              <tfoot style="background-color: #808080; color: white;">

                <tr>
                  <td><strong>รวม</strong></td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td style="padding: 2px">
                    <div style="text-align: right" id="lblSumQty">
                      <?php echo number_format($sum_qty); ?>
                    </div>
                  </td>
                  <td style="padding: 2px">
                    <div style="text-align: right">
                      <?php echo number_format($sum_qty_per_pack); ?>
                    </div>
                  </td>
                  <td style="padding: 2px">
                    <div style="text-align: right" id="lblSumPrice">
                      <?php echo number_format($sum_price); ?>
                    </div>
                  </td>
                  <td></td>
                </tr>

              </tfoot>
            </table>
          </div>
        <?php $this->endWidget(); ?>

        <?php
        $priceVat = 0.00;
        $priceNoVat = number_format($sumTotalPrice);
        $priceTotal = number_format($sumTotalPrice);

        if (!empty($billSaleVat)) {
          if ($billSaleVat == 'vat') {
            // VAT
            $priceVat = number_format($sumTotalPrice * .07, 2);
            $priceNoVat = number_format($sumTotalPrice - ($sumTotalPrice * .07), 2);
            $priceTotal = number_format($sumTotalPrice);
          }
        }
        ?>

      <form class="form-inline">
        <div style="padding-top: 0px">
          <label style="display: inline-block; text-align: left; width: 50px">
            VAT:
          </label>
          <input id="priceVat"
                 type="text"
                 class="form-control disabled"
                 disabled="disabled"
                 value="<?php echo $priceVat; ?>"
                 style="text-align: right; width: 150px" />

          <label style="display: inline-block; width: 120px; text-align: right">
            ราคาไม่รวม VAT:
          </label>
          <input id="priceNoVat"
                 type="text"
                 class="form-control disabled"
                 disabled="disabled"
                 value="<?php echo $priceNoVat; ?>"
                 style="text-align: right; width: 150px" />

          <label style="display: inline-block; width: 100px; text-align: right">
            รวมทั้งสิ้น:
          </label>
          <input id="priceTotal"
                 type="text"
                 class="form-control disabled"
                 disabled="disabled"
                 value="<?php echo $priceTotal; ?>"
                 style="text-align: right; width: 150px" />
        </div>
      </form>

      <div style="color: red; margin-top: 20px; padding-left: 10px; margin-bottom: 20px">
        * จบการขาย กดปุ่ม Control แทนก็ได้
      </div>

      </div>		<!-- panel-body -->
    </div>			<!-- panel -->
    </td>
    <?php
      $products = Product::model()->findAll(array(
        "order" => "product_name",
        "condition" => "product_tag = 1"
      ));
    ?>

    <?php if (count($products) > 0): ?>
    <td width="200px">
      <!-- สินค้าขายบ่อย -->
      <div class="panel panel-primary" style="margin-top: 10px; margin-right: 5px">
        <div class="panel-heading">รายการสินค้าขายบ่อย</div>
        <div class="panel-body" style="padding: 0px; height: 500px; overflow-y: scroll">
          <table class="table table-bordered table-striped">
            <tbody>
              <?php foreach ($products as $product): ?>
              <tr>
                <td><a href="#" onclick="return chooseProduct('<?php echo $product->product_code; ?>')" style="color: black"><?php echo $product->product_name; ?></a></td>
              </tr>
              <?php endforeach; ?>
            </tbody>
          </table>
        </div>
      </div>
    </td>
    <?php endif; ?>
  </tr>
</table>

<!-- Modal -->
<div class="modal" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog" style="width: 850px">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title" id="myModalLabel"></h4>
      </div>
      <div class="modal-body" id="modalContent">

      </div>
      <div class="modal-footer">
        <button id="btnCloseModal" type="button" class="btn btn-danger" data-dismiss="modal">
          <i class="glyphicon glyphicon-remove"></i>
          Close
        </button>
      </div>
    </div>
  </div>
</div>
