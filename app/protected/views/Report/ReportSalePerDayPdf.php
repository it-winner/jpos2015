<?php 
if (empty($result)) {
	echo "<div><strong>ไม่มีข้อมูลในการแสดงรายงาน</strong></div>";
} else {
	include_once '../MPDF57/mpdf.php';
	
	// condition
	if ($sale_condition_cash == 'cash') {
		$sale_condition_cash = 'เงินสด';
	}
	if ($sale_condition_credit == 'credit') {
		$sale_condition_credit = ' เงินเชื่อ';
	}
	
	$branch_name = $branch->branch_name;
	
	// html text
	$html = "
		<style>
			* {
				font-size: 10px;
			}
			.cell-header {
				text-align: center;
				font-weight: bold;
				border-bottom: #808080 3px double;
			}
			.cell {
				padding: 5px;
				border-bottom: #cccccc 1px solid;
			}
			.footer {
				border-bottom: #cccccc 3px double;
				padding: 5px;
			}
		</style>
	
		<div>รายงานยอดขายประจำวัน : {$date_find}</div>
		<div>สาขา: {$branch_name}</div>
		<div>เงื่อนไขการขาย: {$sale_condition_cash} {$sale_condition_credit}</div>
		<br />
	
		<table border='1px'>
			<thead>
				<tr>
					<th width='50px' class='cell-header' style='text-align: left'>ลำดับ</th>
					<th width='80px' class='cell-header' style='text-align: left'>เลขที่บิล</th>
					<th width='100px' class='cell-header' style='text-align: left'>รหัสสินค้า</th>
					<th width='400px' class='cell-header' style='text-align: left'>รายการสินค้า</th>
					<th width='80px' class='cell-header' style='text-align: left'>สถานะบิล</th>
					<th width='130px' class='cell-header' style='text-align: right'>ราคาจำหน่าย/หน่วย</th>
					<th width='100px' class='cell-header' style='text-align: right'>จำหน่ายจริง</th>
					<th width='50px' class='cell-header' style='text-align: right'>กำไรต่อชิ้น</th>
					<th width='50px' class='cell-header' style='text-align: right'>จำนวน</th>
					<th width='80px' class='cell-header' style='text-align: right'>กำไรรวม</th>
					<th width='100px' class='cell-header' style='text-align: right'>จำนวนเงิน</th>
				</tr>
			</thead>

			<tbody>
	";
	
	$html .= "
		</tbody>
	";
	
	$sum = 0;
	$sum_qty = 0;
	$sum_bonus = 0;
	$sum_footer_sum_bill_sale_detail_qty = 0;
	
	// Data
	foreach ($result as $row) {
		$bill_sale_detail_qty = number_format($row['bill_sale_detail_qty']);
		$product_price = number_format($row['product_price'], 2);
		$bill_sale_detail_price = number_format($row['bill_sale_detail_price'], 2);
		
		$change = ($row['product_price'] - $row['bill_sale_detail_price']);
		$change_text = "&nbsp;";
                        	
        if ($change != 0) {
	        $change_text = number_format($change);
        }
		
		$bill_sale_status = $row['bill_sale_status'];
		$price_per_row = ($row['bill_sale_detail_price'] * $row['bill_sale_detail_qty']);
		$price_per_row_text = number_format($price_per_row, 2);
		$bonus = $row['bill_sale_detail_price'] - $row['product_price_buy'];
		
		$sum += $price_per_row;
		$sum_bonus += $bonus;
		$sum_qty += $bill_sale_detail_qty;
		
		$bill_sale_detail_qty = number_format($bill_sale_detail_qty);
		$sum_bill_sale_detail_qty = ($bill_sale_detail_qty * $bonus) ;
		$sum_footer_sum_bill_sale_detail_qty += $sum_bill_sale_detail_qty;
		$sum_bill_sale_detail_qty = number_format($sum_bill_sale_detail_qty, 2);
		$bonus_per_unit = number_format($row['bill_sale_detail_price'] - $row['product_price_buy'], 2);
		
		$html .= "
			<tr>
				<td class='cell'>{$n}</td>
				<td class='cell'>{$row['bill_sale_id']}</td>
				<td class='cell' style='text-align: left'>
                	{$row['bill_sale_detail_barcode']}
                </td>
                <td class='cell' style='text-align: left'>
                    {$row['product_name']}
                </td>
                <td class='cell'>
                	{$bill_sale_status}
                </td>
                <td class='cell' style='text-align: right'>
                	{$product_price}
                </td>
                <td class='cell' style='text-align: right'>
                	{$bill_sale_detail_price}
                </td>
                <td class='cell' style='text-align: right'>
                	{$bonus_per_unit}
                </td>
                <td class='cell' style='text-align: right'>
                	{$bill_sale_detail_qty}
                </td>
                <td class='cell' style='text-align: right'>
                	{$sum_bill_sale_detail_qty}
                </td>
                <td class='cell' style='text-align: right;'>
                	{$price_per_row_text}
                </td>
			</tr>";
			
		$n++;
	}
	
	$sum_qty = number_format($sum_qty);
	$sum_bonus = number_format($sum_bonus, 2);
	$sum = number_format($sum, 2);
	$sum_footer_sum_bill_sale_detail_qty = number_format($sum_footer_sum_bill_sale_detail_qty, 2);

	$html .= "
		<tfoot>
			<tr>
				<td colspan='7'>รวม</td>
				<td class='footer' style='text-align: right'></td>
				<td class='footer' style='text-align: right'>{$sum_qty}</td>
				<td class='footer' style='text-align: right'>{$sum_footer_sum_bill_sale_detail_qty}</td>
				<td class='footer' style='text-align: right'>{$sum}</td>
			</tr>	
		</tfoot>
	</table>
	";
	
	// Generate PDF
	$mpdf = new mPDF('UTF-8', 'A4-L', 0, 0, 5, 5, 5, 5);
	$mpdf->SetAutoFont();
	$mpdf->WriteHTML($html);
	$mpdf->Output();
}
?>
