<?php if (Yii::app()->request->cookies['user_id'] != null): ?>

<?php
$user_id = Yii::app()->request->cookies['user_id']->value;
$user = User::model()->findByPk($user_id);

$this->widget("application.extensions.mbmenu.MbMenu", array(
  "items" => array(
    array("label" => 'บันทึกประจำวัน', "items" => array(
      array("label" => "ขายสินค้า", "url" => array("Basic/Sale")),
      array("label" => "รับคืนสินค้า", "url" => array("Basic/GetSale")),
      array("label" => "จัดการบิลขาย", "url" => array("Basic/ManageBill")),
      array("label" => "ซ่อมแซมสินค้า", "url" => array("Basic/Repair")),
      array("label" => "รับซ่อมสินค้าภายนอก", "url" => array("Basic/GetRepair")),
      array("label" => "รับเข้าสินค้า", "url" => array("Basic/BillImport")),
      array("label" => "ใบวางบิล", "url" => array("Basic/BillDrop")),
      array("label" => "ใบเสนอราคา", "url" => array("Basic/BillQuotation")),
      array("label" => "เช็คสต็อก", "url" => array("Basic/CheckStock")),
      array("label" => "เปลี่ยนรหัสผ่าน", "url" => array("Basic/ChangeProfile")),
      array("label" => "พักหน้าจอ", "url" => array("Site/Home"))
    )),
    array("label" => "รายงาน", "visible" => (@$user->user_level == "admin"), "items" => array(
        array(
          "label" => "รายงานยอดขาย",
          "items" => array(
            array("label" => "ยอดขายประจำวัน", "url" => array("Report/SalePerDay")),
            array("label" => "สรุปยอดขายตามวัน", "url" => array("Report/SaleSumPerDay")),
            array("label" => "สรุปยอดขายตามเดือน", "url" => array("Report/SaleSumPerMonth")),
            array("label" => "สรุปยอดขายตามประเภท", "url" => array("Report/SaleSumPerType")),
            array("label" => "สรุปยอดขายตามสมาชิก", "url" => array("Report/SaleSumPerMember")),
            array("label" => "สรุปยอดขายตามพนักงาน", "url" => array("Report/SaleSumPerEmployee"))
          )
        ),
        array("label" => "รายงานสินค้า", "url" => array("Report/ProductStock")),
        array("label" => "รายงานลูกหนี้", "url" => array("Report/ReportAR")),
        array("label" => "รายงานเจ้าหนี้", "url" => array("Report/ReportIR"))
    )),
    array(
      "label" => "ส่งเสริมการขาย", "visible" => (@$user->user_level == "admin"), "items" => array(
         array("label" => "กำหนดส่วนลด", "url" => array("Support/SubPrice"))                                                                                        
      )
    ),
    array("label" => "ตั้งค่าพื้นฐาน", "visible" => (@$user->user_level == "admin"), "items" => array(
        array("label" => "ข้อมูลร้านค้า", "url" => array("Config/Organization")),
        array("label" => "คลังสินค้า/สาขา", "url" => array("Config/BranchIndex")),
        array("label" => "ประเภทสินค้า", "url" => array("Config/GroupProductIndex")),
        array("label" => "สินค้า", "url" => array("Config/ProductIndex")),
        array("label" => "ตัวแทนจำหน่าย", "url" => array("Config/FarmerIndex")),
        array("label" => "สมาชิกร้าน", "url" => array("Config/MemberIndex")),
        array("label" => "ผู้ใช้งานระบบ", "url" => array("Config/UserIndex")),
        array("label" => "ตั้งค่าการพิมพ์บิล", "url" => array("Config/BillConfigIndex")),
        array("label" => "เงินในลิ้นชักวันนี้", "url" => array("Config/DrawcashSetup"))
    )),
    array('label' => 'ช่วยเหลือ', 'visible' => (@$user->user_level == 'admin'), 'items' => array(
      array('label' => 'update โปรแกรม', 'url' => array('Help/UpdateSoftware')),
      array('label' => 'เกี่ยวกับเรา', 'url' => array('Help/About'))
    ))
)));

?>

<?php endif; ?>
