<?php

class HelpController extends Controller {

  public function actionUpdateSoftware() {
    $this->render("//Help/UpdateSoftware");
  }

  public function actionAbout() {
    $this->render("//Help/About");
  }

  public function actionUpdateSoftwareNow() {
    $sql = "
      ALTER TABLE tb_bill_sale_detail 
      CHANGE bill_sale_detail_price bill_sale_detail_price DOUBLE
    ";
    Yii::app()->db->createCommand($sql)->execute();

    $sql = "
      CREATE TABLE IF NOT EXISTS product_prices(
        order_field INT(2) NOT NULL,
        product_barcode VARCHAR(20) NOT NULL,
        qty INT(5) NOT NULL DEFAULT 0,
        price DOUBLE NOT NULL DEFAULT 0,
        price_send DOUBLE NOT NULL DEFAULT 0
      )
    ";
    Yii::app()->db->createCommand($sql)->execute();

    // add column
    try {
      $sql = "ALTER TABLE tb_product ADD product_tag INT(1) DEFAULT 0 NOT NULL";
      Yii::app()->db->createCommand($sql)->execute();
    } catch (Exception $e) {

    }

    try {
      $sql = "ALTER TABLE tb_product ADD product_pic VARCHAR(255) NULL";
      Yii::app()->db->createCommand($sql)->execute();
    } catch (Exception $e) {

    }

    // add column
    try {
      $sql = "ALTER TABLE tb_product ADD weight INT(11) DEFAULT 0 NOT NULL";
      Yii::app()->db->createCommand($sql)->execute();
    } catch (Exception $e) {

    }

    // add column
    try {
      $sql = "ALTER TABLE product_prices ADD qty_end INT(5) DEFAULT 0 NOT NULL";
      Yii::app()->db->createCommand($sql)->execute();
    } catch (Exception $e) {

    }

    $sql = "
      CREATE TABLE IF NOT EXISTS drawcash_logs(
        id INT(11) PRIMARY KEY AUTO_INCREMENT NOT NULL,
        draw_date DATETIME,
        draw_price DOUBLE DEFAULT 0 NOT NULL
      )
    ";
    Yii::app()->db->createCommand($sql)->execute();

     // CREATE TABLE
      $sql = "
          CREATE TABLE IF NOT EXISTS quotations (
            id INT(11) PRIMARY KEY AUTO_INCREMENT NOT NULL,
            customer_name VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
            customer_address VARCHAR(1000) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
            customer_tel VARCHAR(50) CHARACTER SET utf8 COLLATE utf8_general_ci,
            customer_fax VARCHAR(50) CHARACTER SET utf8 COLLATE utf8_general_ci,
            customer_tax VARCHAR(13) CHARACTER SET utf8 COLLATE utf8_general_ci,
            quotation_day INT(3) NOT NULL,
            quotation_send_day INT(3) NOT NULL,
            quotation_pay ENUM('cash', 'credit') CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
            created_at DATETIME NOT NULL,
            user_id INT(11) NOT NULL,
            vat INT(2) NOT NULL
          )
        ";
        Yii::app()->db->createCommand($sql)->execute();

        $sql = "
          ALTER TABLE quotations 
          DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci
        ";
        Yii::app()->db->createCommand($sql)->execute();

    // CREATE TABLE quotation_details
    $sql = "
      CREATE TABLE IF NOT EXISTS quotation_details (
        id INT(11) PRIMARY KEY AUTO_INCREMENT NOT NULL,
        quotation_id INT(11) NOT NULL,
        barcode VARCHAR(255) NOT NULL,
        old_price FLOAT NOT NULL,
        qty INT(5) NOT NULL,
        sub INT(11) NOT NULL,
        sale_price FLOAT NOT NULL
      )
    ";

    Yii::app()->db->createCommand($sql)->execute();

    $sql = "
      CREATE TABLE IF NOT EXISTS backgrounds(
        id INT(11) PRIMARY KEY AUTO_INCREMENT NOT NULL,
        name VARCHAR(500) NOT NULL,
        background_default ENUM('yes', 'no') DEFAULT 'no' NOT NULL
      )
    ";
    Yii::app()->db->createCommand($sql)->execute();

    // add column
    try {
      $sql = "ALTER TABLE tb_organization ADD logo_show_on_header ENUM('no', 'yes') DEFAULT 'no' NOT NULL";
      Yii::app()->db->createCommand($sql)->execute();
    } catch (Exception $e) {

    }

    // add column
    try {
      $sql = "ALTER TABLE tb_organization ADD logo_show_on_header_bg ENUM('no', 'yes') DEFAULT 'no' NOT NULL";
      Yii::app()->db->createCommand($sql)->execute();
    } catch (Exception $e) {

    }

    $sql = "
      CREATE TABLE IF NOT EXISTS barcode_prices(
        barcode VARCHAR(50) PRIMARY KEY NOT NULL,
        price DOUBLE NOT NULL DEFAULT 0,
        name VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_general_ci,
        qty_sub_stock INT(5) DEFAULT 1 NOT NULL,
        barcode_fk VARCHAR(50) NOT NULL
      )
    ";
    Yii::app()->db->createCommand($sql)->execute();

    try {
      $sql = "ALTER TABLE tb_repair ADD repair_group ENUM('internal', 'external') DEFAULT 'internal' COMMENT 'สินค้าซ่อมมาจากร้านหรือไม่ internal คือสินค้าในร้าน external คือสินค้านอกร้าน' NOT NULL";
      Yii::app()->db->createCommand($sql)->execute();
    } catch (Exception $e) {

    }

    try {
      $sql = "ALTER TABLE tb_repair ADD repair_tel VARCHAR(50) COMMENT 'เบอร์โทรติดต่อ' NOT NULL";
      Yii::app()->db->createCommand($sql)->execute();
    } catch (Exception $e) {

    }

    try {
      $sql = "ALTER TABLE tb_repair ADD repair_name VARCHAR(255) COMMENT 'ลูกค้า' NOT NULL";
      Yii::app()->db->createCommand($sql)->execute();
    } catch (Exception $e) {

    }

    try {
      $sql = "ALTER TABLE tb_repair ADD repair_product_name VARCHAR(255) COMMENT 'สินค้า' NOT NULL";
      Yii::app()->db->createCommand($sql)->execute();
    } catch (Exception $e) {

    }

    try {
      $sql = "ALTER TABLE tb_repair ADD repair_end_date DATETIME COMMENT 'วันที่ซ่อมเสร็จ' NULL";
      Yii::app()->db->createCommand($sql)->execute();
    } catch (Exception $e) {

    }

    try {
      $sql = "ALTER TABLE tb_bill_config ADD slip_font_size INT(2) COMMENT 'ขนาดตัวอักษร' NOT NULL DEFAULT 11";
      Yii::app()->db->createCommand($sql)->execute();
    } catch (Exception $e) {

    }

    try {
      $sql = "ALTER TABLE tb_bill_import_detail ADD import_bill_detail_code VARCHAR(50) COMMENT 'รหัสสินค้า ที่รับเข้า' NULL";
      Yii::app()->db->createCommand($sql)->execute();
    } catch (Exception $e) {

    }

    try {
      $sql = "ALTER TABLE tb_bill_import_detail ADD import_bill_detail_qty_per_pack INT(2) COMMENT 'จำนวนต่อแพค' NULL";
      Yii::app()->db->createCommand($sql)->execute();
    } catch (Exception $e) {

    }

    //
    // TODO: Message
    //
    Yii::app()->user->setFlash("message", "update software เรียบร้อยแล้ว");

    $this->render("//Help/UpdateSoftware");
  }

}