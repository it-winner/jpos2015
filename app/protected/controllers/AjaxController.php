<?php

@ob_start();

class AjaxController extends Controller {

  public function actionGetGroupProductInfo($group_product_code) {
    $attributes = array();
    $attributes["group_product_code"] = $group_product_code;

    $model = GroupProduct::model()->findByAttributes($attributes);
    echo CJSON::encode($model);
  }

  public function actionGenProductCode() {
    $varTime = microtime();
    $varTime = str_replace(" ", "", $varTime);
    $varTime = str_replace(".", "", $varTime);

    echo $varTime;
  }

  public function actionSaveProduct() {
    if (!empty($_POST)) {
      $model = new Product();
      $model->attributes = $_POST["Product"];

      if ($model->save()) {
        echo "success";
      }
    }
  }

  public function actionGetProductInfo($product_code) {
    $condition = array();
    $condition["product_code"] = $product_code;

    $model = Product::model()->findByAttributes($condition);

    if (empty($model)) {
      // find by barcode_price
      $barcodePrice = BarcodePrice::model()->findByAttributes(array(
        'barcode' => $product_code
      ));

      $model = $barcodePrice->getProduct();

      if (!empty($model)) {
        $model->product_name = $model->product_name.' : '.$barcodePrice->name.' จำนวน '.$barcodePrice->qty_sub_stock.' ชิ้น';
        $model->product_total_per_pack = $barcodePrice->qty_sub_stock; 
      }
    }
    echo CJSON::encode($model);
  }

  public function actionPrintBarCode($barcode = null) {
    $barcodeObj = new Barcode39($barcode);
    $barcodeObj->draw();
  }

  public function actionSaleSaveOnGrid() {
    $billSaleDetail = Yii::app()->session['billSaleDetail'];

    $prices = $_POST['prices'];
    $qtys = $_POST['qtys'];
    $serials = $_POST['serials'];

    // remove product item from array
    for ($i = 0; $i < count($billSaleDetail); $i++) {
      $product_code = $billSaleDetail[$i]['product_code'];

      $product = Product::model()->findByAttributes(array(
          'product_code' => $product_code
      ));

      $billSaleDetail[$i]['product_serial_no'] = $serials[$i];
      $billSaleDetail[$i]['product_price'] = $prices[$i];
      $billSaleDetail[$i]['product_qty'] = $qtys[$i];

      if ($prices[$i] < $product->product_price) {
        $billSaleDetail[$i]['has_bonus'] = 'yes';
      } else {
        $billSaleDetail[$i]['has_bonus'] = 'no';
      }
    }

    Yii::app()->session['billSaleDetail'] = $billSaleDetail;
  }

  public function actionConvertNumberToText($number) {
    echo Util::convertNumberToText($number);
  }

  public function actionBillSaleDetail($bill_sale_id) {
    $dataProvider = new CActiveDataProvider('BillSaleDetail', array(
        'criteria' => array(
            'condition' => "bill_id = $bill_sale_id",
            'order' => 'bill_sale_detail_id DESC'
        ),
        'pagination' => false
    ));

    $this->renderPartial('//Ajax/BillSaleDetail', array(
        'dataProvider' => $dataProvider,
        'bill_sale_id' => $bill_sale_id
    ));
  }

  public function actionSearchMemberByMemberCode($member_code) {
    $member = Member::model()->findByAttributes(array(
      "member_code" => $member_code
    ));

    echo CJSON::encode($member);
  }

  public function actionGetUserInfo($user_id) {
    if (!empty($user_id)) {
      $user = User::model()->findByPk($user_id);
      echo CJSON::encode($user);
    }
  }

}
