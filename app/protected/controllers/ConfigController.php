<?php

class ConfigController extends Controller {

  function checkLogin() {
    if (Yii::app()->request->cookies['user_id'] == null) {
      $this->redirect(array("Site/Index"));
    }
  }

  function actionOrganization() {   
    $this->checkLogin(); 

    $model = Organization::model()->find();

    if (!empty($_POST)) {
      $model->attributes = $_POST["Organization"];

      // show logo on bill
      $org_logo_show_on_bill = "no";
      $logo_show_on_header = "no";
      $logo_show_on_header_bg = "no";

      if (!empty($_POST['logo_show_on_header'])) {
        $logo_show_on_header = $_POST['logo_show_on_header'];
      }
      if (!empty($_POST['org_logo_show_on_bill'])) {
        $org_logo_show_on_bill = $_POST['org_logo_show_on_bill'];
      }
      if (!empty($_POST['logo_show_on_header_bg'])) {
        $logo_show_on_header_bg = $_POST['logo_show_on_header_bg'];
      }

      $on_bill = 'no';

      if ($org_logo_show_on_bill == 1) {
        $on_bill = 'yes';
      }

      $model->org_logo_show_on_bill = $on_bill;
      $model->logo_show_on_header = $logo_show_on_header;
      $model->logo_show_on_header_bg = $logo_show_on_header_bg;

      // logo
      $org_logo = CUploadedFile::getInstance($model, 'org_logo');
      $old_logo = $model->org_logo;

      if (!empty($org_logo)) {
        $model->org_logo = $org_logo;
        $model->org_logo->saveAs('upload/' . $org_logo);

        // remove old logo
        if (file_exists('upload/' . $old_logo)) {
          @unlink('upload/' . $old_logo);
        }
      }

      $model->save();
    }
    $this->render("//Config/Organization", array('model' => $model));
  }

  function actionBranchIndex() {
    $this->checkLogin();

    $model = new Branch();
    $this->render("//Config/BranchIndex", array('model' => $model));
  }

  function actionBranchForm($id = null) {
    $this->checkLogin();

    $model = new Branch();

    if (!empty($_POST)) {
      $pk = $_POST["Branch"]["branch_id"];

      if (!empty($pk)) {
        $model = Branch::model()->findByPk($pk);
      }
      $model->attributes = $_POST["Branch"];

      if ($model->save()) {
        $this->redirect(array('BranchIndex'));
      }
    }

    if ($id != null) {
      $model = Branch::model()->findByPk($id);
    }
    $this->render('//Config/BranchForm', array('model' => $model));
  }

  function actionBranchDelete($id) {
    $this->checkLogin();

    Branch::model()->deleteByPk($id);
    $this->redirect(array('BranchIndex'));
  }

  function actionGroupProductIndex() {
    $this->checkLogin();

    $model = new GroupProduct();
    $this->render('//Config/GroupProductIndex', array('model' => $model));
  }

  function actionGroupProductForm($id = null) {
    $this->checkLogin();

    $model = new GroupProduct();

    if (!empty($_POST)) {
      $pk = $_POST["GroupProduct"]["group_product_id"];

      if (!empty($pk)) {
        $model = GroupProduct::model()->findByPk($pk);
      }
      $model->attributes = $_POST["GroupProduct"];

      if ($model->save()) {
        $this->redirect(array('GroupProductIndex'));
      }
    }

    if (!empty($id)) {
      $model = GroupProduct::model()->findByPk($id);
    }

    $this->render('//Config/GroupProductForm', array(
      'model' => $model
    ));
  }

  function actionGroupProductDelete($id) {
    $this->checkLogin();

    GroupProduct::model()->deleteByPk($id);
    $this->redirect(array('GroupProductIndex'));
  }

  function actionProductIndex($productTag = false, $search = null) {
    $this->checkLogin();

    $condition = "product_id > 0";

    if ($productTag) {
      $condition .= " AND product_tag = 1";
    }

    $params = array();

    if (!empty($search)) {
      $condition .= " 
        AND 
        (
          product_name LIKE(:search)
          OR product_code LIKE(:search)
        )
      ";
      $params['search'] = '%'.$search.'%';
    }

    if (!empty($_GET['Product_sort'])) {
      $order = $_GET['Product_sort'];
      $order = str_replace(".desc", " DESC", $order);
    } else {
      $order = "product_id DESC";
    }

    $model = new CActiveDataProvider("Product", array(
      "criteria" => array(
        "condition" => $condition,
        "order" => $order,
        "params" => $params
      ),
    ));
    
    $this->render('//Config/ProductIndex', array(
      'model' => $model,
      'search' => $search
    ));
  }

  function actionProductForm($id = null) {
    $this->checkLogin();

    $model = new Product();
    $default_product_expire = 'expire';
    $default_product_return = 'in';
    $default_product_sale_condition = 'sale';

    if (!empty($_POST)) {
      $pk = $_POST["Product"]["product_id"];

      if (!empty($pk)) {
        $model = Product::model()->findByPk($pk);
      }

      // group_product_code to group_product_id
      $pk = $_POST['Product']['group_product_id'];
      $groupProduct = GroupProduct::model()->findByAttributes(array(
        "group_product_code" => $pk
      ));

      if (empty($groupProduct)) {
        $groupProduct = GroupProduct::model()->findByPk($pk);
      }

      $model->attributes = $_POST["Product"];
      $model->group_product_id = $groupProduct->group_product_id;
      $model->weight = $_POST['weight'];

      if (!empty($_POST['product_tag'])) {
        $model->product_tag = $_POST['product_tag'];
      } else {
        $model->product_tag = 0;
      }

      // process total small unit
      if (!empty($_POST['Product']['product_quantity_of_pack'])) {
        $total = ($model->product_total_per_pack * $model->product_quantity_of_pack);
        $model->product_quantity = $total;
      }

      // product_expire_date
      if (!empty($_POST['Product']['product_expire_date'])) {
        $product_expire_date = $_POST['Product']['product_expire_date'];
        $model->product_expire_date = Util::thaiToMySQLDate($product_expire_date);
      }

      // save image of product and upload
      if ($_FILES['product_pic']['name'] != "") {
        $name = $_FILES['product_pic']['name'];
        $tmp = $_FILES['product_pic']['tmp_name'];

        $ext = explode(".", $name);
        $ext = $ext[count($ext) - 1];

        $ext = strtolower($ext);

        if ($ext == "png" || $ext == "jpg") {
          $name = microtime();
          $name = str_replace(" ", "", $name);
          $name = str_replace("0.", "", $name);
          $name = $name.".".$ext;

          if (move_uploaded_file($tmp, "upload/$name")) {
            // remove old image
            if (!empty($model->product_pic)) {
              $oldImg = $model->product_pic;

              if (file_exists("upload/$oldImg")) {
                unlink("upload/$oldImg");
              }
            }

            // set new image
            $model->product_pic = $name;
          }
        }
      }

      // save
      if ($model->save()) {
        $this->redirect(array('ProductIndex'));
      }
    }

    $barcodePrices = null;

    if (!empty($id)) {
      $model = Product::model()->findByPk($id);
      $model->product_expire_date = Util::mysqlToThaiDate($model->product_expire_date);

      $barcodePrices = BarcodePrice::model()->findAllByAttributes(array(
        'barcode_fk' => $model->product_code
      ));
    } else {
      $model->product_total_per_pack = 1;
    }

    $params['model'] = $model;
    $params['default_product_expire'] = $default_product_expire;
    $params['default_product_return'] = $default_product_return;
    $params['default_product_sale_condition'] = $default_product_sale_condition;
    $params['barcodePrices'] = $barcodePrices;

    $this->render('//Config/ProductForm', $params);
  }

  function actionProductDelete($id) {
    $this->checkLogin();

    Product::model()->deleteByPk($id);
    $this->redirect(array('ProductIndex'));
  }

  function actionFarmerIndex() {
    $this->checkLogin();

    $model = new Farmer();
    $this->render('//Config/FarmerIndex', array(
      'model' => $model
    ));
  }

  function actionFarmerForm($id = null) {
    $this->checkLogin();

    $model = new Farmer();

    if (!empty($_POST)) {
      $pk = $_POST['Farmer']['farmer_id'];

      if (!empty($pk)) {
        $model = Farmer::model()->findByPk($pk);
      }
      $model->attributes = $_POST['Farmer'];

      if ($model->save()) {
        $this->redirect(array('FarmerIndex'));
      }
    }

    if (!empty($id)) {
      $model = Farmer::model()->findByPk($id);
    }
    $this->render('//Config/FarmerForm', array(
      'model' => $model
    ));
  }

  function actionFarmerDelete($id) {
    $this->checkLogin();

    Farmer::model()->deleteByPk($id);
    $this->redirect(array('FarmerIndex'));
  }

  function actionMemberIndex() {
    $this->checkLogin();

    $model = new Member();
    $this->render('//Config/MemberIndex', array(
      'model' => $model
    ));
  }

  function actionMemberForm($id = null) {
    $this->checkLogin();

    $model = new Member();

    if (!empty($_POST)) {
      $pk = $_POST['Member']['member_id'];

      if (!empty($pk)) {
        $model = Member::model()->findByPk($pk);
      }

      $model->attributes = $_POST["Member"];

      // save branch_id
      if (empty($_POST['Member']['branch_id'])) {
        $user_id = Yii::app()->request->cookies['user_id']->value;
        $user = User::model()->findByPk($user_id);

        $model->branch_id = $user->branch_id;
      }

      if ($model->save()) {
        $this->redirect(array('MemberIndex'));
      }
    }

    if (!empty($id)) {
      $model = Member::model()->findByPk($id);
    }

    $this->render('//Config/MemberForm', array(
      'model' => $model
    ));
  }

  function actionMemberDelete($id = null) {
    $this->checkLogin();

    Member::model()->deleteByPk($id);
    $this->redirect(array('MemberIndex'));
  }

  function actionUserIndex() {
    $this->checkLogin();

    $model = new User();
    $this->render('//Config/UserIndex', array(
      'model' => $model
    ));
  }

  function actionUserForm($id = null) {
    $this->checkLogin();

    $model = new User();

    if (!empty($_POST)) {
      $pk = $_POST["User"]["user_id"];
      if (!empty($pk)) {
        $model = User::model()->findByPk($id);
      }

      $model->attributes = $_POST["User"];

      if ($model->save()) {
        $this->redirect(array('UserIndex'));
      }
    }

    if (!empty($id)) {
      $model = User::model()->findByPk($id);
    }
    $this->render('//Config/UserForm', array(
      'model' => $model
    ));
  }

  function actionUserDelete($id) {
    $this->checkLogin();

    User::model()->deleteByPk($id);
    $this->redirect(array('UserIndex'));
  }

  function actionBillConfigIndex() {
    $this->checkLogin();

    $billConfig = BillConfig::model()->find();

    if (empty($billConfig)) {
      $billConfig = new BillConfig();
    }

    // Save
    if (!empty($_POST)) {
      $billConfig->_attributes = $_POST['BillConfig'];

      if ($billConfig->save()) {
        $this->redirect(array('BillConfigIndex'));
      }
    }

    $this->render('//Config/BillConfigIndex', array(
      'billConfig' => $billConfig
    ));
  }

  function actionAbout() {
    $this->checkLogin();
    $this->render('//Config/About');
  }

  function actionProductClear() {
    $this->checkLogin();

    Product::model()->deleteAll();
    $this->redirect(array("ProductIndex"));
  }

  function actionProductPriceSave() {
    $this->checkLogin();

    if (!empty($_POST)) {
      $product_code = $_POST['product_code'];
      $product_prices = str_replace(",", "", $_POST['product_price']);
      $product_price_sends = str_replace(",", "", $_POST['product_price_send']);
      $qtys = $_POST['qty'];
      $qty_ends = $_POST['qty_end'];
      $i = 0;

      ProductPrice::model()->deleteAllByAttributes(array(
        "product_barcode" => $product_code
      ));

      foreach ($product_prices as $product_price) {
        $price = $product_price;
        $price_send = $product_price_sends[$i];
        $qty = $qtys[$i];
        $qty_end = $qty_ends[$i];

        if ($qty > 0 && $price > 0) {
          $productPrice = new ProductPrice();
          $productPrice->product_barcode = $product_code;
          $productPrice->price = $price;
          $productPrice->price_send = $price_send;
          $productPrice->qty = $qty;
          $productPrice->qty_end = $qty_end;
          $productPrice->order_field = ($i + 1);
          $productPrice->save();
        }

        $i++;
      }

      echo 'success';
    }
  }

  public function actionDrawcashSetup() {
    $this->checkLogin();

    if (!empty($_POST)) {
      if (!empty($_POST['draw_price'])) {
        $drawCash = new DrawCash();
        $drawCash->draw_price = $_POST['draw_price'];
        $drawCash->draw_date = new CDbExpression("NOW()");
        
        if ($drawCash->save()) {
          $this->redirect(array("DrawcashSetup"));
        }
      }
    }

    $drawCashs = new CActiveDataProvider('DrawCash', array(
      'criteria' => array(
        "order" => "id DESC",
        "limit" => 30
      )
    ));

    $this->render("//Config/Drawcash", array(
      'drawCashs' => $drawCashs
    ));
  }

  public function actionDrawcashDelete($id) {
    $this->checkLogin();
    
    Drawcash::model()->deleteByPk($id);
    $this->redirect(array("DrawcashSetup"));
  }

  public function actionSaveProductPriceBarCode() {
    $this->checkLogin();

    if (!empty($_POST)) {
      $barcodes = $_POST['barcode'];
      $prices = $_POST['price'];
      $qtys = $_POST['qty'];
      $names = $_POST['name'];
      $barcode_fk = $_POST['product_code'];

      if (!empty($barcodes)) {
        $size = count($barcodes);

        // delete
        BarcodePrice::model()->deleteAllByAttributes(array(
          'barcode_fk' => $barcode_fk
        ));

        // insert
        for ($i = 0; $i < $size; $i++) {
          if ($prices[$i] != 0) {
            $barcodePrice = new BarcodePrice();
            $barcodePrice->barcode = $barcodes[$i];
            $barcodePrice->price = $prices[$i];
            $barcodePrice->qty_sub_stock = $qtys[$i];
            $barcodePrice->name = $names[$i];
            $barcodePrice->barcode_fk = $barcode_fk;
            $barcodePrice->save();
          }
        }

        echo 'success';
      }
    }
  }

}
